package co.jbink.mustacheee.lights;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import Objects.Controller;

public class ControllerActivity extends AppCompatActivity {

    Controller controller;
    TextView ipAddress_TextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = this.getIntent().getExtras();
        this.controller = new Controller(bundle.getString("ipAddress", "Default"));
        this.ipAddress_TextView = (TextView) findViewById(R.id.controller_ipAddress);
        ipAddress_TextView.setText(controller.getIpAddress());
    }

}
