package Objects;

/**
 * Created by benja on 9/13/2016.
 */
public class Controller {
    private String ipAddress;

    public Controller(String ipAddress){
        this.ipAddress = ipAddress;
    }

    public String getIpAddress(){
        return this.ipAddress;
    }
}
